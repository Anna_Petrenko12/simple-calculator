package com.example.calculate;


import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;

import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity{

    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView((R.layout.activity_main));
    }
    public void onButtonClick(View v) {
        EditText element1 = (EditText) findViewById(R.id.etNum1);
        EditText element2 =  (EditText) findViewById(R.id.etNum2);
        EditText operator =  (EditText) findViewById(R.id.operator);
        TextView resText=(TextView) findViewById(R.id.result);
        String action=operator.getText().toString();
       // Button button = (Button) findViewById(R.id.button);
        // button.setOnClickListener(new View.OnClickListener() {
        int number1=0;
        int number2=0;
        try {
            number1 = Integer.parseInt(element1.getText().toString());
            number2 = Integer.parseInt(element2.getText().toString());
        }catch (Exception e) {
                Toast.makeText(getApplicationContext(), "Нужно ввести число", Toast.LENGTH_SHORT);
                return;
        }

        double res;
        switch (action)
        {
            case "+":
                res=number1 + number2;
                resText.setText(Double.toString(res));
                break;
            case "-":
                res= number1 - number2;
                resText.setText(Double.toString(res));
                break;
            case "*":
                res= number1 * number2;
                resText.setText(Double.toString(res));
                break;
            case "/":
                if (number2 != 0) {
                    res= number1 / number2;
                } else{
                    resText.setText("Нельзя делить на ноль");
                     return;
                }
                resText.setText(Double.toString(res));
                break;
            default:resText.setText("Wrong data");
                                      }


                                      }
    }


  


